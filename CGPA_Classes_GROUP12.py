'''
GROUP 12 MEMBERS:
    TUHAISE EUGENE (23/U/18012/PS)
    KATENDE RONNIE MAGALA (23/U/09239/PS)
    NDYAMANYI MARVIN SATULO (21/U/10072/PS)
    SERUGO SIMON PETER (18/U/25565/EVE)
'''

# Part(b)
class calculator:
    @staticmethod
    def add(arg):
        return sum(arg)
    @staticmethod   
    def mult(*arg):
        product = 1
        for num in arg:
            product *= num
        return product
    @staticmethod
    def div(*arg):
        try:
            quotient = arg[0]
            for num in arg[1:]:
                quotient /= num
            return quotient
        except ZeroDivisionError:
            print(("Cannot divide by zero!!!").upper())
class CGPACalculator(calculator):
    @staticmethod
    def calculate_cgpa(marks,__grades,__cred_units):
        calc = calculator()
        tot_cred_units = calc.add(__cred_units)
        weight_sum = calc.add([calc.mult(CGPACalculator.grade_pts(cred_pt),credit) for cred_pt,credit in zip(__grades,__cred_units)])
        cgpa = calc.div(weight_sum,tot_cred_units)
        return cgpa
    @staticmethod
    def grade_pts(grade):
        grade_pts = {'A+':5.0,'A':5.0,'B+':4.5,'B':4.0,'C+':3.5,'C':3.0,'D+':2.5,'D':2.0,'E':1.5,'E-':1.0,'F':0.0}
        return grade_pts[grade]
class BasicCalc(calculator):
    @staticmethod
    def calculate_sum(student_num):
        calc = calculator()
        num = str(student_num)
        vals = [int(num[i:i+2]) for i in range(1, len(num)-1,2)]
        # vals.remove(8)
        return calc.add(vals)
def append_to_word(file,content):
    with open(file,'a') as fx:
        fx.write(content)
student1,student2,student3,student4 = (2300718012,2100710072,1800725565,2300709239) 
stud_num = student1+student2+student3+student4
num = str(stud_num)
myMarks = BasicCalc.calculate_sum(stud_num)
myMarks = [int(num[i:i+2]) for i in range(1, len(num),2)]
myMarks.remove(myMarks[4])
CU1,CU2,CU3,CU4 = myMarks
print(myMarks)
def get_grade(mark):
    my_grades = []
    for i in mark:
        if i >= 90:
            my_grades.append('A+')
        elif i >= 80:
            my_grades.append('A')
        elif i >= 75:
            my_grades.append('B+')
        elif i >= 70:
            my_grades.append('B')
        elif i >= 65:
            my_grades.append('C+')
        elif i >= 60:
            my_grades.append('C')
        elif i >= 55:
            my_grades.append('D+')
        elif i >= 50:
            my_grades.append('D')
        elif i >= 45:
            my_grades.append('E')
        elif i >= 40:
            my_grades.append('E-')
        else:
            my_grades.append('F')
    return tuple(my_grades)
myGrade = get_grade(myMarks)
myCreditUnits = [4,4,4,4]
output_partB = f'Task(b)(using Classes):-\nCGPA Results: {CGPACalculator.calculate_cgpa(myMarks,myGrade,myCreditUnits):.2f}\n'
old_output_file = 'CGPA_results Group 12.doc'
new_data = f'\n\n  {output_partB}'
print(CGPACalculator.calculate_cgpa(myMarks,myGrade,myCreditUnits))
append_to_word(old_output_file,new_data)